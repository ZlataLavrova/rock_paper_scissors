from typing import Any, Dict

import requests


def get_seed() -> str:
    return input("Please enter seed:\t")


def get_score_table(seed: str, url: str) -> Dict[str, Any]:
    return requests.get(f"{url}/{seed}").json()


def handle_exception(exc: Dict[str, Any]) -> None:
    """Можно сделать декоратор, а можно его и не делать)"""
    if not exc["status"]:
        print(exc["reason"])


def print_score_table(resp: Dict[str, Any]):
    print(resp["game_field"][0][0], "- Admin")
    print(resp["game_field"][0][1], "- Guest\n")


def make_move(value: str, seed: str, url: str, admin: bool) -> Dict[str, Any]:
    return requests.post(
        f"{url}/{seed}", json={"value": value, "admin": admin}
    ).json()


def take_results(seed: str, url: str) -> Dict[str, Any]:
    return requests.get(f"{url}/{seed}results").json()


def main(url: str):
    seed = get_seed()
    field_resp = get_score_table(seed=seed, url=url)
    handle_exception(field_resp)
    if field_resp["game_field"][0][0] != 0 or field_resp["game_field"][0][1] != 0 or field_resp["game_field"][2] > 2:
        print("You are a spectator")
        print_score_table(field_resp)
        results = field_resp
        if results["game_field"][0][0] == 3:
            print("Admin won in this game")
            main(url=url)
        elif results["game_field"][0][1] == 3:
            print("Guest won in this game")
            main(url=url)
        while results["game_field"][0][0] != 3 or results["game_field"][0][1] != 3:
            try:
                n = input("Press enter to update the results")
            finally:
                results = take_results(seed=seed, url=url)
                print_score_table(results)
                if results["game_field"][0][0] == 3:
                    print("Admin won in this game")
                    main(url=url)
                elif results["game_field"][0][1] == 3:
                    print("Guest won in this game")
                    main(url=url)
    results = field_resp
    print("HowToPlay:\tR - Rock, P - Paper, S - Scissors\n")
    print_score_table(field_resp)
    while results["game_field"][0][0] != 3 or results["game_field"][0][1] != 3:
        value_chapter = input("Type R/P/S:\t").upper()
        resp = make_move(
            value=value_chapter, seed=seed, url=url, admin=bool(field_resp["admin"])
        )
        handle_exception(resp)
        results = take_results(seed=seed, url=url)
        handle_exception(results)
        print_score_table(results)
        if results["game_field"][0][0] == 3:
            print("Admin won")
            main(url=url)
        elif results["game_field"][0][1] == 3:
            print("Guest won")
            main(url=url)


if __name__ == "__main__":
    main(url="http://localhost:5000")

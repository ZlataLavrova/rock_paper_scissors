from flask import Flask, jsonify, request


app = Flask(__name__)


database = {}


@app.route("/")
def homepage():
    return "homepage"


@app.route("/<string:game_seed>", methods=["GET"])
def enter_in_game(game_seed: str):
    if not database.get(game_seed, None):
        database[game_seed] = [[0, 0], [None, None], 1]
        return jsonify(game_field=database[game_seed], status=True, admin=True)
    database[game_seed][2] += 1
    print(database)
    return jsonify(game_field=database[game_seed], status=True, admin=False)


@app.route("/<string:game_seed>", methods=["POST"])
def make_move(game_seed: str):
    while database[game_seed][1][0] is None or database[game_seed][1][1] is None:
        req = request.get_json()
        if req["admin"] is True:
            database[game_seed][1][0] = req["value"]
        else:
            database[game_seed][1][1] = req["value"]
    move1 = database[game_seed][1][0]
    move2 = database[game_seed][1][1]
    try:
        if (move1.upper() not in ["R", "P", "S"]) or (move2.upper() not in ["R", "P", "S"]):
            return jsonify(status=False, reason="Someone made an incorrect move")
        if move1 == move2:
            return jsonify(game_field=database[game_seed][0], status=False, reason="Items are equal")
    except KeyError as e:
        return jsonify(status=False, reason=e)
    except ValueError as e:
        return jsonify(status=False, reason=e)
    else:
        if req["admin"] is True:
            return jsonify(game_field=win_conditions(move1, move2, game_seed), status=True)
        else:
            return jsonify(game_field=database[game_seed][0], status=True)


@app.route("/<string:game_seed>results", methods=["GET"])
def give_scoreboard(game_seed: str):
    database[game_seed][1] = [None, None]
    return jsonify(game_field=database[game_seed], status=True)


def win_conditions(move1, move2, game_seed):
    if (move1.upper() == "R" and move2.upper() == "S") \
            or (move1.upper() == "P" and move2.upper() == "R") \
            or (move1.upper() == "S" and move2.upper() == "P"):
        database[game_seed][0][0] += 1
    if (move2.upper() == "R" and move1.upper() == "S") \
            or (move2.upper() == "P" and move1.upper() == "R") \
            or (move2.upper() == "S" and move1.upper() == "P"):
        database[game_seed][0][1] += 1
    return database[game_seed][0]


if __name__ == "__main__":
    app.run()
